package com.example.myapplication.model

import com.google.gson.annotations.SerializedName


data class MoviePageResponse(
    @SerializedName("page") val pageNumber: Int,
    @SerializedName("results") val movies: List<MovieResponse>,
    @SerializedName("total_pages") val totalPagesAmount: Int,
    @SerializedName("total_results") val totalMoviesAmount: Int
)

data class MovieResponse(
    @SerializedName("poster_path") val posterPath: String?,
    @SerializedName("adult") val isAdult: Boolean,
    @SerializedName("overview") val overview: String,
    @SerializedName("release_date") val releaseDate: String,
    @SerializedName("genre_ids") val genres: List<Int>,
    @SerializedName("id") val id: Int,
    @SerializedName("original_title") val originalTitle: String,
    @SerializedName("original_language") val originalLanguage: String,
    @SerializedName("title") val title: String,
    @SerializedName("backdrop_path") val backdropPath: String?,
    @SerializedName("popularity") val popularity: Double,
    @SerializedName("vote_count") val voteCount: Int,
    @SerializedName("video") val isVideo: Boolean,
    @SerializedName("vote_average") val voteAverage: Double
)


data class Movie(
    val posterPath: String?,
    val isAdult: Boolean,
    val overview: String,
    val releaseDate: String,
    val genres: List<Int>,
    val id: Int,
    val originalTitle: String,
    val originalLanguage: String,
    val title: String,
    val backdropPath: String?,
    val popularity: Double,
    val voteCount: Int,
    val isVideo: Boolean,
    val voteAverage: Double
)