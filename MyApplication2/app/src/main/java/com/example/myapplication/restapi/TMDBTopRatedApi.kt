package com.example.myapplication.restapi

import com.example.myapplication.model.MoviePageResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiClient {

    @GET("movie/{movie_id}/recommendations")
    suspend fun fetchMovieRecommendations(
        @Path("movie_id") movieId: Int
    ): MoviePageResponse

}