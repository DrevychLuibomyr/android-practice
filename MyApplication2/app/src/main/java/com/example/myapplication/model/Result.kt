package com.example.myapplication.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Result<T> : Serializable {
    @SerializedName("response")
    val data: T? = null
    @SerializedName("error")
    val error: Error? = null
}