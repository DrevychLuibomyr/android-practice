package com.example.myapplication.viewModel

import androidx.lifecycle.MutableLiveData
import com.example.myapplication.model.MovieResponse

class MainActivvityViewModel: BaseViewModel() {

    val liveData = MutableLiveData<Event<MovieResponse>>()

    fun getMovies(movieID: Int) {
        requestWithLiveData(liveData) {
            api.fetchMovieRecommendations(movieID)
        }
    }

}