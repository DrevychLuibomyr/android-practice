package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.myapplication.model.MovieResponse
import com.example.myapplication.viewModel.MainActivvityViewModel
import com.example.myapplication.viewModel.Status

class MainActivity : AppCompatActivity() {

    private lateinit var activityViewModel: MainActivvityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        activityViewModel = ViewModelProviders.of(this).get(MainActivvityViewModel::class.java)
        observeGetRequest()
    }

    private fun observeGetRequest() {
        activityViewModel.liveData.observe(this, Observer {
            when (it.status) {
                Status.LOADING -> showLoading()
                Status.SUCCESS -> viewOneSuccess(it.data)
                Status.ERROR -> show(it.error)
            }
        })
    }

    private fun viewOneSuccess(data: MovieResponse?) {
       Log.v(this.toString(), data.toString())
    }


    private fun show(error: Error?) {
        Toast.makeText(this, error?.localizedMessage, Toast.LENGTH_LONG).show()
    }

    private fun showLoading() {
        Toast.makeText(this, "Loading", Toast.LENGTH_LONG).show()
    }
}
