package com.example.myapplication.restapi

import com.example.myapplication.BuildConfig
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import java.util.concurrent.TimeUnit


object ApiClientFactory {

    private val TMDB_BASE_URL = "https://api.themoviedb.org/3/"

    private val loggingInterceptor = run {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.apply {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }
    }

    public fun retrofitService(): ApiClient {
        val interceptor = insertInterceptor()
        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(interceptor)
            .addInterceptor(loggingInterceptor)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .build()

        val retrofit = Retrofit.Builder()
            .baseUrl(TMDB_BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        return retrofit.create(ApiClient::class.java)
    }

    private fun insertInterceptor() = Interceptor {
        val url = it.request().url
            .newBuilder()
            .addQueryParameter("apiKey", "a0f7a5f06093cb51b3e9b0319b88ba87")
            .build()

        val newRequest = it.request()
            .newBuilder()
            .url(url)
            .build()

        it.proceed(newRequest)
    }
}




