package com.example.myapplication.Utils

import com.example.myapplication.model.Movie
import com.example.myapplication.model.MovieResponse

fun MovieResponse.toMovie() = Movie(
    posterPath = posterPath,
    isAdult = isAdult,
    overview = overview,
    releaseDate = releaseDate,
    genres = genres,
    id = id,
    originalTitle = originalTitle,
    originalLanguage = originalLanguage,
    title = title,
    backdropPath = backdropPath,
    popularity = popularity,
    voteCount = voteCount,
    isVideo = isVideo,
    voteAverage = voteAverage
)